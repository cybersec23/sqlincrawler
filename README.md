# Crawler
_Author: Jakub Skrzyński_

This software is developed as a modular library. One may use whole package, adjust it to own needs, or develop own modules that follow certain interfaces and include them in own instance of crawler.

Simplest usage is presented in listing below:
```python
c = Crawler("http://localhost/cs", BasicExtractor(BasicInjectionTester()), 1)
c.start()
print(c.get_last_results())
```

## Standard mechanism

Crawler performs its operations sequentially. On each level of the search tree for each endpoint it performs:

 - Check if visited
 - Mark as visited
 - Querying
 - Searching for new endpoints (Calls `BaseEndpointExtractor.extract_endpoints()`)
     - Extract from `<a>` (implemented by `BasicExtractor`)
     - Extract from `<form>` (implemented by `BasicExtractor`)
     - Extract from text (implemented by `BasicExtractor`)
 - Test discovered endpoints for potential injections (Calls `BaseEndpointExtractor.extract_injection_endpoints()`)
 - Add discovered endpoints to batch for next level
 - Save visited endpoints
 - Save discovered endpoints
 - Save potential injection points
 - Save visited urls

Instance of crawler is created by providing following parameters:

```python
    seed_url: str                           #Url to start from
    base_extractor: BaseEndpointExtractor   #Instance of class responsible for extracting endpoints and injection points
    depth: int                              #Depth of crawling
```

The constructor is shown below

```python
def __init__(self, seed_url: str, base_extractor: BaseEndpointExtractor, depth: int)
```

### Depth of crawling

Depth identifies number of edges between root and last processed child. For example setting the value to 0 ends the search after processing seed url while value 1 will allow to process seed url, go to every of its endpoints and process these endpoints.

---

# Standard filters

Crawler is prepared in such a fashion that it should be easily customizable but apart from that it comes with set of ready filters that based on server response are capable of extracting endpoints (link with connection method and list of parameters as well as their default values). Sections below describes how general mechanism works as well as how the basic implementation is done.

## Endpoint extraction and discovery of potential injection points

Extraction is performed with use of class that implements interface `BaseEndpointExtractor`

```python
class BaseEndpointExtractor(ABC):

    @abstractmethod
    def extract_injection_endpoints(self, response: Response = None, list_of_endpoints: List[EndpointCall] = []) -> List[EndpointCall]:
        pass

    @abstractmethod
    def extract_endpoints(self, response: Response) -> List[EndpointCall]:
        pass
```

Function `extract_injection_endpoints` should accept `Response` object that will be converted to list of endpoints or a collection of ready `EndpointCall` to be tested. __Only one of the args should be provided.__

The above requirement can be realised by use of following code snippet:

```python
if response is None and len(list_of_endpoints) == 0:
    raise Exception("No endpoints")
if response is not None and len(list_of_endpoints) > 0:
    raise Exception("Provide response or list! not both")
```

Crawler calls the extractor the way with ready list of endpoints, not providing the response. Reason of such behaviour is that endpoints are already extracted using `extract_endpoints` that is a method of `BaseEndpointExtractor`. One may change this behaviour by editing `start` function of class `Crawler`. One should comment first line and uncomment the second one in listing below.

```python
discovered_potential_injection_points = self._base_extractor.extract_injection_endpoints(list_of_endpoints=discovered_ec_temp)
# discovered_potential_injection_points = self._base_extractor.extract_injection_endpoints(response=resp)
```

## BasicExtractor

This is a standard implementation of `BaseEndpointExtractor`. It includes functionalities as extracting endpoints from tags `<a>`, forms and text presented on website. In package there is present standard injection tester but as it is complex task that may need improvements and adjustments, there is provided a handle to provide own tester.

```python
class BasicExtractor(BaseEndpointExtractor):

    _injection_tester: BaseInjectionTester

    def __init__(self, injection_tester: BaseInjectionTester):
        self._injection_tester = injection_tester
```

### extract_endpoints()

This method implements functionalities mentioned below:
- Extract from `<a>` 
- Extract from `<form>`
- Extract from text

### extract_injection_endpoints()

This is a method responsible for testing if some endpoint may be vulnerable to SQL injection. To do so it uses `test_endpoint` from `BaseInjectionTester`. It calls the method with an endpoint and if the method returns `True` endpoint is added to the list of injections.

## Injection testing - Theory

SQL injection is a vulnerability that is caused mostly by poor input validation - namely when user is able to pass SQL code to database. To test against such vulnerability by hand we usually try to type by hand string that may result in such behaviour into forms, urls or request body. Such string should contain proper termination of present query. Below pseudocode causing such vulnerability is presented.

```pseudocode
query("SELECT * FROM users WHERE username = '" + username + "' AND pass = '" + pass + "'")
```

Inserting password `password' OR 1 --` would cause the query to malfunction, as it will start to look as fallows:

```SQL
SELECT * FROM users WHERE username = 'user' AND pass = 'password' OR 1 --'
```

Results of such execution may differ among systems, as it really depends on a way system reads db records processes them and prepares output. Such lack of standard reaction makes it hard to automatize the whole process.

One may observe that ome measure of success in injection may be behaviour different than the normal one. Having that assumption it is possible to use it as estimator of injection success. Namely we can perform a couple of requests with data that resembles real data, store results (or some values describing them), then try to execute queries that will contain injections.

The job of estimating if the injection was indeed successful - response differs much from normal ones - can be completed based on few values:

- Matching size of normal responses with the one from injection test - If there is a significant change in its size there is high probability that something worked differently in system than it used to do.
- Matching the hash of responses contents - if hashes differ the response is for sure different.

Both of above can result in lots of false positives, because site may contain for example advertisements that are injected into its code on server side. Then number of chars in response as well as its hash will for sure differ.

Nevertheless, this seems to be the simplest and fastest method to mark the suspicious endpoints but it can be treated only as suspicious, not the "for sure" vulnerable. Final check must be performed by crawler operator.

As an additional measure of injection success one may try to look for n error message in the returned response, but the error message may never appear when some safety measures were taken by the developer.

Another measure can be the response time - inject into query a function that will cause the database to wait before answering and measure the response time. If it is higher or matches the one specified in query it meas that it got executed. It is used mostly in a blind injections where we have no visual indication that something had been executed. Most important in this method is selecting correct timeout to make it not blend into normal network delay and to remin efficient.

Methods described above are used in default implementation.

## BaseInjectionTester

It is an interface that states the way `BasicExtractor` interact with object responsible for testing endpoints for injection.

```python
class BaseInjectionTester(ABC):
    @abstractmethod
    def test_endpoint(self, endpoint: EndpointCall) -> bool:
        pass
```

One willing to customize the process should implement the object that implements this interface and pass it to `BasicExtractor` 

## BasicInjectionTester

It is build in implementation of `BaseInjectionTester` making the crawler ready to be used. This component needs to be tuned to the environment it is going to be run in. Especially parameters connected with tolerance of time differences between declared and measured values.

It operates on three basic principles

- Measurement of request answer time
- Comparing size of response
- Searching for error messages

### Time measurement

Function that realizes this functionality takes the endpoint specification `EndpointCall`. It fills all possible arguments with one of selected strings from objects internal array. If at least one string is finally executed by db it should cause delay specified in this string. Injection strings usually coment out the remaining query part making it meaningless so first executed delay cancels out the rest. Time is then averaged between `retries_of_one_string` retries. Mentioned tolerance is defined by `tolerance_factor`.

Lower and upper bounds are computed based on parameters mentioned above. If some string has average result higher than lower bound it gets accepted. Effect of exceeding the upper bound is a warning. It means that it is potentially an injection point but the delay may be also a result of an error.

Time of single endpoint testing is: 

$$
retries_of_one_string \cdot in_query_delay + len(_delay_strings) \cdot normal\_delay
$$

Testing is stopped after finding first string that makes the endpoint wait for the given time.

If using this implementation for larger systems one must take into consideration probable time of execution and probably adjust the default parameters to shorten the overall time.

*Note: Current implementation on injection strings require specific vulnerability to exist, namely requests should be possible to be executed sequentially in one command (`query1;query2;query3`) to support ather ways of execution you may edit current strings or add new ones*

Injection strings that are used are stored in `BaseInjectionTester` field `_delay_strings`

### Response size comparison

Firstly the system generates a call filled with random strings of given length. It executes it once and then jumps to injections stored in `BaseInjectionTester._injection_strings`. For each string function fills all call arguments with this string and executes the call. Size of the response is then compared with size of response filled with random strings with a given tolerance. If size exceeds this tolerance it is reported as probable injection point.

### Search for key phrases

Call is executed with each possible corruption string that is stored in `BaseInjectionTester._injection_corruption_strings`. Response is then converted to raw text and system checks if any of strings present in `BaseInjectionTester._corruption_signs` appears in answer. If there is at least one match function returns `True`

### Adjustments

Class `BaseInjectionTester` has a full interface to introduce changes in mentioned settings. Listing below shows all necessary functions that are members of that class. Each function has clear description of the functionality and all parameters

```python
    def set_delay_test_params(self, in_query_delay: float = 10.0, tolerance_factor: float = 0.05, retries_of_one_string: int = 4):
        """
        Function sets parameters of delay testing filter
        :param in_query_delay: Number of seconds delay in _delay_strings
        :param tolerance_factor: Possible difference from the declared value expressed as a fraction -> 0.05 is 5%. If in tolerance limit then it is marked as detected
        :param retries_of_one_string: Numbers of retries of a single measurement to get the average
        :return:
        """
        self._delay_test_in_query_delay = in_query_delay
        self._delay_test_tolerance_factor = tolerance_factor
        self._delay_test_retries_of_one_string = retries_of_one_string

    def set_size_test_params(self, fill_random: bool = False, random_len: int = 10, size_diff_tolerance: float = 0.1):
        """
        Function sets parameters of size testing filter
        :param fill_random: If true arguments are filled with random values. If false they are left empty
        :param random_len: Size of random strings the normal request is filled with
        :param size_diff_tolerance: Possible difference from the declared value expressed as a fraction -> 0.05 is 5%. If in tolerance limit it is marked as a normal fluctuation
        :return: No return
        """
        self._size_test_fill_random = fill_random
        self._size_test_random_len = random_len
        self._size_test_size_diff_tolerance = size_diff_tolerance

    def set_injection_strings(self, injection_strings: List[str]):
        """
        Function sets array of strings that is used during the size test
        :param injection_strings: Array of strings that will be passed to size test as possible injections
        :return:
        """
        self._injection_strings = injection_strings

    def set_injection_corruption_strings(self, injection_corruption_strings: List[str]):
        """
        Function sets array of strings that are used during testing for corruption of query
        :param injection_corruption_strings: Array of strings that are used during testing for corruption of query
        :return:
        """
        self._injection_corruption_strings = injection_corruption_strings

    def set_corruption_signs(self, corruption_signs: List[str]):
        """
        Function sets array of signs of corruption of query
        :param corruption_signs: Array of signs of corruption of query. If any of strings in this table is dettected in response then it is assumed that query was corrupted
        :return:
        """
        self._corruption_signs = corruption_signs

    def set_delay_strings(self, delay_strings: List[str]):
        """
        Function sets array of strings that are used during delay test as injections
        :param delay_strings: Array of strings that are used during delay test as injections
        :return: 
        """
        self._delay_strings = delay_strings

    def set_base_str_for_random_str(self, base_str_for_random_str: str):
        """
        Function sets the string that contains alphabet for random string generation
        :param base_str_for_random_str: String that contains alphabet for random string generation
        :return:
        """
        self._base_str_for_random_str = base_str_for_random_str
```

---

# Example
In catalog `php-example` there is provided vulnerable php code that the crawler can be tested on. It implements simple query to mysql db.

---

# Dependencies

```
beautifulsoup4==4.12.2
certifi==2023.5.7
charset-normalizer==3.1.0
idna==3.4
requests==2.30.0
soupsieve==2.4.1
urllib3==2.0.2
```

---

# Final notes

This mechanism should never be run on production machines. It generates a lot of traffic and may polute the database with meaningless records. Furthermore, using it on computer that belongs to third parties withut propper permission may be considered as illegal.

The tool is provided **ONLY** to discover vulnerabilities in test environment running without any connection to production machines.

There is absolutely no guarantee that tool will not break the instance of system. Operator should take into consideration that it performs really aggressive scrapping and testing.

Before using review the testing routine - default one may impose huge load on database server. Default tests execute series of queries to measure estimators meaning that server should be prepared to withstand numerous queries per single tested endpoints. Queries include delay functions that may result in blocking server resources (Not safe for production!)

**Author takes no responsibility for illegal or improper use of this software - it is just a tool, and it is up to you how it is used!!!**

*Whole description is provided for educational purpose and author does not hold any responsibility for misuse*

