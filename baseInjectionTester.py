from abc import abstractmethod, ABC

from endpointCall import EndpointCall


class BaseInjectionTester(ABC):
    @abstractmethod
    def test_endpoint(self, endpoint: EndpointCall) -> bool:
        pass
