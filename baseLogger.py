from enum import Enum


class MessageType(Enum):
    INFO = 0
    ERROR = 1
    WARNING = 2