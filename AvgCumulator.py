class AvgCumulator:
    _counter: int
    _cumulative: float

    def __init__(self):
        self._cumulative = 0.0
        self._counter = 0

    def add(self, value: float):
        self._cumulative += value
        self._counter += 1

    def get(self):
        return self._cumulative / self._counter
