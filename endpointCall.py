import urllib
from typing import List

from requests import Response, Request, Session


class EndpointCall:
    _resourceUrl: str
    _method: str # 'GET' 'POST'
    _argsNames: List[str]
    _urlArgsNames: List[str]
    _argsValues: List[str]
    _urlArgsValues: List[str]

    def __init__(self, url, method, arg_names=[], url_args_names=[], arg_values=[], url_arg_values=[]):
        self._resourceUrl = url
        self._method = method
        self._argsNames = arg_names
        self._argsValues = arg_values
        self._urlArgsNames = url_args_names
        self._urlArgsValues = url_arg_values
        pass

    def __str__(self) -> str:
        result = {
            "_resourceUrl": self._resourceUrl,
            "_method": self._method,
            "_argsNames": self._argsNames,
            "_argsValues": self._argsValues,
            "_urlArgsNames": self._urlArgsNames,
            "_urlArgsValues": self._urlArgsValues
        }
        return str(result)

    def __repr__(self) -> str:
        return self.__str__()

    def get_nice_string(self):
        result = "---------------\n"
        result += "Url: "+self._resourceUrl+"\n"
        result += "Method: "+self._method+"\n"
        result += "In url args: " + str(self._urlArgsNames) + "\n"
        result += "Request body args: " + str(self._argsNames) + "\n"
        result += "---------------\n"
        return result
    def set_argsValues(self,argsValues):
        self._argsValues = argsValues

    def set_urlArgsValues(self,urlArgsValues):
        self._urlArgsValues = urlArgsValues

    def getDefaultUrl(self) -> str:
        url = self._resourceUrl + '?' + urllib.parse.urlencode(
            self._prepare_payload(self._urlArgsNames, self._urlArgsValues))
        return url

    def getUrlArgsCount(self) -> int:
        return len(self._urlArgsNames)

    def getArgsCount(self) -> int:
        return len(self._argsNames)

    def executeRequest(self, arg_values: List[str] = None, url_arg_values: List[str] = None) -> Response:
        """
        Function that executes the request with given arg values
        :param url_arg_values: values of arguments in url, index should match index of names
        :param arg_values: values of arguments in body, index should match index of names
        :return:
        """
        if arg_values is None:
            arg_values = self._argsValues
        if url_arg_values is None:
            url_arg_values = self._urlArgsValues

        url = self._resourceUrl + '?' + urllib.parse.urlencode(self._prepare_payload(self._urlArgsNames,url_arg_values))
        s = Session()
        r = Request(self._method, url, data=self._prepare_payload(self._argsNames, arg_values))
        pr = r.prepare()
        resp = s.send(pr)
        # print(url)
        return resp

    def _prepare_payload(self, arg_names: List[str], arg_values: List[str]):
        payload = {}

        if len(arg_values) > len(arg_names):
            raise Exception("Invalid number of parameters")

        for i in range(min(len(arg_names), len(arg_values))):
            payload[arg_names[i]] = arg_values[i]

        return payload
