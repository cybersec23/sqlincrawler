from abc import ABC, abstractmethod
from typing import List

from requests import Response

from endpointCall import EndpointCall


class BaseEndpointExtractor(ABC):

    @abstractmethod
    def extract_injection_endpoints(self, response: Response = None, list_of_endpoints: List[EndpointCall] = []) -> List[EndpointCall]:
        pass

    @abstractmethod
    def extract_endpoints(self, response: Response) -> List[EndpointCall]:
        pass