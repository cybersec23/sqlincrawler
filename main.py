from BasicExractor import BasicExtractor
from BasicInjectionTester import BasicInjectionTester
from crawler import Crawler
from endpointCall import EndpointCall

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    c = Crawler("http://localhost/cs", BasicExtractor(BasicInjectionTester()), 1)
    c.start()
    print(c.get_last_results())
    c.print_last_results()
