from typing import List, Set

from baseEndpointExtractor import BaseEndpointExtractor
from baseLogger import MessageType
from endpointCall import EndpointCall


class Crawler:
    _seed_url: str
    _base_extractor: BaseEndpointExtractor
    _depth: int  # last level to which we dig

    _last_visited_ec: List[EndpointCall]
    _last_discovered_ec: List[EndpointCall]
    _last_injection_ec: List[EndpointCall]
    _last_visited_urls: List[str]

    def __init__(self, seed_url: str, base_extractor: BaseEndpointExtractor, depth: int) -> None:
        self._seed_url = seed_url
        self._base_extractor = base_extractor
        self._depth = depth

        self._last_visited_ec = []
        self._last_discovered_ec = []
        self._last_injection_ec = []
        self._last_visited_urls = []

    def _log(self, message: str, message_type: MessageType = MessageType.INFO):
        print("[Crawler]", message)

    def start(self):
        """
        Starts crawler routine
        :return:
        """
        start_ec = EndpointCall(self._seed_url, "GET")

        visited_urls: Set[str] = set()
        visited_ec: List[EndpointCall] = []
        discovered_ec: List[EndpointCall] = []
        injection_points: List[EndpointCall] = []

        ec_for_current_level: List[EndpointCall] = []
        ec_for_current_level += [start_ec]
        for level in range(self._depth + 1):
            ec_for_next_level = []
            level_ec: EndpointCall
            for level_ec in ec_for_current_level:
                if level_ec.getDefaultUrl() not in visited_urls:
                    self._log("Processing " + level_ec.getDefaultUrl())
                    visited_urls.add(level_ec.getDefaultUrl())
                    try:
                        resp = level_ec.executeRequest()

                        discovered_ec_temp = self._base_extractor.extract_endpoints(resp)
                        discovered_potential_injection_points = self._base_extractor.extract_injection_endpoints(list_of_endpoints=discovered_ec_temp)
                        # discovered_potential_injection_points = self._base_extractor.extract_injection_endpoints(response=resp)



                        ec_for_next_level += discovered_ec_temp
                        discovered_ec += discovered_ec_temp
                        injection_points += discovered_potential_injection_points
                        visited_ec += [level_ec]
                    except Exception as e:
                        self._log("Error while handling " + level_ec.getDefaultUrl() + " ! " + str(e))
            ec_for_current_level.clear()
            ec_for_current_level += ec_for_next_level
            self._log("Finished level " + str(level))

        self._last_visited_ec = visited_ec
        self._last_injection_ec = injection_points
        self._last_visited_urls = list(visited_urls)
        self._last_discovered_ec = discovered_ec

    def get_last_results(self):
        """
        Returns results of last crawler operations
        :return:
        """
        return {"visited_ec": self._last_visited_ec, "discovered_ec": self._last_discovered_ec, "injection_ec": self._last_injection_ec, "visited_urls": self._last_visited_urls}

    def print_last_results(self):
        print("################################")
        print("Crawler report")
        print("################################")
        print("Visited endpoints")
        ec: EndpointCall
        for ec in self._last_visited_ec:
            print(ec.get_nice_string())
        print("################################")
        print("During visiting of this endpoints, endpoints presented below were discovered\n(It cointains also visited ones)")
        print("--------------------------------")
        ec: EndpointCall
        for ec in self._last_discovered_ec:
            print(ec.get_nice_string())
        print("################################")
        print("Anomalies were detected on following endpoints\nYou should check them")
        print("--------------------------------")
        ec: EndpointCall
        for ec in self._last_injection_ec:
            print(ec.get_nice_string())
        print("################################")
        print("Below all visited urls are presented")
        print("--------------------------------")
        for url in self._last_visited_urls:
            print(url)
        print("################################")
        print("End of Crawler report")
        print("################################")
