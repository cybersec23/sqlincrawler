import copy
import re
import urllib
from typing import List
from urllib.parse import urlparse, parse_qs, ParseResult, urlunsplit

from bs4 import BeautifulSoup, SoupStrainer
from requests import Response

from baseEndpointExtractor import BaseEndpointExtractor
from baseInjectionTester import BaseInjectionTester
from endpointCall import EndpointCall


class BasicExtractor(BaseEndpointExtractor):

    _injection_tester: BaseInjectionTester

    def __init__(self, injection_tester: BaseInjectionTester):
        self._injection_tester = injection_tester

    def extract_injection_endpoints(self, response: Response = None, list_of_endpoints: List[EndpointCall] = []) -> List[EndpointCall]:
        if response is None and len(list_of_endpoints) == 0:
            raise Exception("No endpoints")
        if response is not None and len(list_of_endpoints) > 0:
            raise Exception("Provide response or list! not both")

        endpoints: List[EndpointCall] = []

        if response is not None:
            endpoints = self.extract_endpoints()
        if len(list_of_endpoints) > 0:
            endpoints = list_of_endpoints

        marked: List[EndpointCall] = []

        for ec in endpoints:
            if self._injection_tester.test_endpoint(ec):
                marked += [ec]

        return marked

    def extract_endpoints(self, response: Response) -> List[EndpointCall]:
        result = []
        result += self._bs_a_extractor(response)
        result += self._re_text_extractor(response)
        result += self._bs_form_extractor(response)
        return result

    def _trim_query(self, parsed_url: ParseResult) -> str:
        return urlunsplit((parsed_url.scheme, parsed_url.netloc, parsed_url.path, "", ""))

    def _convert_url_to_endpoint(self, raw_link_str: str, response: Response, method: str = "GET",
                                 arg_names: List[str] = [], arg_values: List[str] = []):
        parse_url = urlparse(raw_link_str)
        dict_result = parse_qs(parse_url.query, keep_blank_values=True)
        names = list(dict_result.keys())
        values = [dict_result[x][0] for x in dict_result.keys()]
        url_relative = self._trim_query(parse_url)
        url = ""
        if url_relative != "":
            url = urllib.parse.urljoin(response.url, url_relative)
        final_parsed_link = urlparse(url)
        if final_parsed_link.scheme == "http" or final_parsed_link.scheme == "https":
            ec = EndpointCall(url, method, url_args_names=names, url_arg_values=values, arg_names=arg_names,
                              arg_values=arg_values)
            return ec
        return None

    def _bs_form_extractor(self, response: Response) -> List[EndpointCall]:

        soup = BeautifulSoup(response.text, parse_only=SoupStrainer('form'), features="html.parser")
        result = []
        for form in soup:
            form_action = response.url
            form_names = []
            form_values = []
            form_method = "POST"

            if form.has_attr('action'):
                form_action = form['action']

            if form.has_attr('method'):
                form_method = form['method']

            # find all inputs
            # for input in form.find_all(lambda tag: tag.name == "input" or tag.name == "textarea"):
            def extract_tag_type(tags):
                for input in tags: # form.find_all("input"):
                    if input.has_attr('name'):
                        input_name = input['name']
                        input_val = None
                        if input.has_attr('value'):
                            input_val = input['value']
                        form_names.append(input_name)
                        form_values.append(input_val)

            extract_tag_type(form.find_all("input"))
            extract_tag_type(form.find_all("textarea"))

            ec = self._convert_url_to_endpoint(form_action, response, method=form_method, arg_names=form_names,
                                               arg_values=form_values)
            if ec is not None:
                result.append(ec)
        return result

    def _bs_a_extractor(self, response: Response) -> List[EndpointCall]:
        links_from_tag_a = []
        soup = BeautifulSoup(response.text, parse_only=SoupStrainer('a'), features="html.parser")
        for link in soup:
            if link.has_attr('href'):
                raw_url = link['href']
                ec = self._convert_url_to_endpoint(raw_url, response)
                if ec is not None:
                    links_from_tag_a.append(ec)
        return links_from_tag_a

    def _re_text_extractor(self, response: Response) -> List[EndpointCall]:
        END_PUNCTUATION = r"[\.\?>\"'\)!,}:;\u201d\u2019\uff1e\uff1c\]]*"
        SEPARATOR_DEFANGS = r"[\(\)\[\]{}<>\\]"
        URL_SPLIT_STR = r"[>\"'\),};]"

        link_text_pattern = re.compile(r"""
                (
                    # Scheme.
                    [fhstu]\S\S?[px]s?
                    # One of these delimiters/defangs.
                    (?:
                        :\/\/|
                        :\\\\|
                        :?__
                    )
                    # Any number of defang characters.
                    (?:
                        \x20|
                        """ + SEPARATOR_DEFANGS + r"""
                    )*
                    # Domain/path characters.
                    \w
                    \S+?
                    # CISCO ESA style defangs followed by domain/path characters.
                    (?:\x20[\/\.][^\.\/\s]\S*?)*
                )
            """ + END_PUNCTUATION + r"""
                (?=\s|$)
            """, re.IGNORECASE | re.VERBOSE | re.UNICODE)

        link_in_text = link_text_pattern.findall(self._bs_get_text(response.text))
        links_from_text = []
        for link in link_in_text:
            ec = self._convert_url_to_endpoint(link, response)
            if ec is not None:
                links_from_text.append(ec)
        return links_from_text

    def _bs_get_text(self, html_str: str):
        soup = BeautifulSoup(html_str, 'html.parser')
        text = soup.find_all(text=True)

        output = ''
        blacklist = [
            '[document]',
            'noscript',
            'header',
            'html',
            'meta',
            'head',
            'input',
            'script',
        ]

        for t in text:
            if t.parent.name not in blacklist:
                output += '{} '.format(t)
        return output
